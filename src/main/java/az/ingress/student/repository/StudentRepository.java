package az.ingress.student.repository;

import az.ingress.student.model.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Optional<Student> findByFirstNameStartsWith(String predicate);


}
