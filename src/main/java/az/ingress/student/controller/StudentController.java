package az.ingress.student.controller;

import az.ingress.student.config.AppConfig;
import az.ingress.student.dto.StudentDto;
import az.ingress.student.service.StudentInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("student")
@RequiredArgsConstructor
public class StudentController {

    private final AppConfig appConfig;

    private final StudentInfoService studentInfoService;

    @GetMapping("/details")
    public String getAppDetails() {
        appConfig.getDevelopers().stream().forEach(System.out::println);
        appConfig.getNumbers().forEach((k, v) -> System.out.println(k + "=" + v));
        return appConfig.getName() + " My Version:" + appConfig.getName();

    }

    @GetMapping
    public StudentDto getStudent(@RequestParam Long id) {
        return studentInfoService.findStudentById(id);
    }

    @PostMapping
    public void createStudent(@RequestBody StudentDto student) {
        studentInfoService.createStudent(student);
    }
}
