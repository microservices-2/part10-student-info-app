package az.ingress.student.service;

import az.ingress.student.dto.StudentDto;
import az.ingress.student.exceptionns.StudentNotFoundException;
import az.ingress.student.model.Student;
import az.ingress.student.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentInfoService {

    private final StudentRepository studentRepository;
    private final ModelMapper mapper;

    public void createStudent(StudentDto studentDto) {
        studentRepository.save(mapper.map(studentDto, Student.class));
    }

    public Student findStudent(String prefix) {
        return studentRepository.findByFirstNameStartsWith(prefix)
                .orElseThrow(() -> new RuntimeException("Nothing is found"));
    }

    public StudentDto findStudentById(Long id) {
        return studentRepository.findById(id)
                .map((s) -> mapper.map(s, StudentDto.class))
                .orElseThrow(StudentNotFoundException::new);
    }
}
